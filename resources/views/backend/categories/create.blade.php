<x-backend.master>
    <x-slot:title>
        Categories Create
    </x-slot:title>
    <main id="main" class="main">

<form action="{{ route('categories.store') }}" method="post">
    @csrf
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Name</label>
          <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
          <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
        </div>

        <div class="mb-3 form-check">
          <input type="checkbox" name="is_active" class="form-check-input" id="exampleCheck1">
          <label class="form-check-label" for="exampleCheck1">Check me out</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
</form>
    </main>
</x-backend.master>
