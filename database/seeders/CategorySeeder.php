<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::Create([
            'name' => 'Ingratiants',
            'is_active' => true
        ]);
        Category::Create([
            'name' => 'Fruits',
            'is_active' => true
        ]);
        Category::Create([
            'name' => 'Vegetables',
            'is_active' => true
        ]);
        Category::Create([
            'name' => 'Drinks',
            'is_active' => true
        ]);
    }
}
